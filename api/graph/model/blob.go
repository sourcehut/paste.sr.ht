// This type is not rigged up with GraphQL, and is only used internally.
package model

type Blob struct {
	ID  int
	SHA string
}
