"""Add OAuthToken table

Revision ID: 4e79c2cb0648
Revises: 5ddda71f7dd5
Create Date: 2019-03-03 10:08:14.772176

"""

# revision identifiers, used by Alembic.
revision = '4e79c2cb0648'
down_revision = '5ddda71f7dd5'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('oauthtoken',
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("created", sa.DateTime, nullable=False),
        sa.Column("updated", sa.DateTime, nullable=False),
        sa.Column("expires", sa.DateTime, nullable=False),
        sa.Column("token_hash", sa.String(128), nullable=False),
        sa.Column("token_partial", sa.String(8), nullable=False),
        sa.Column("scopes", sa.String(512), nullable=False),
        sa.Column("user_id", sa.Integer, sa.ForeignKey('user.id')))


def downgrade():
    op.drop_table('oauthtoken')
