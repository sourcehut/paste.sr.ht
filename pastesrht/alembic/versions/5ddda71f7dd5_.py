"""Strip empty filenames from DB

Revision ID: 5ddda71f7dd5
Revises: None
Create Date: 2019-02-26 07:15:04.730520

"""

# revision identifiers, used by Alembic.
revision = '5ddda71f7dd5'
down_revision = None

from alembic import op
import sqlalchemy as sa

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session as BaseSession, relationship

Session = sessionmaker()
Base = declarative_base()

class PasteFile(Base):
    __tablename__ = 'paste_file'
    id = sa.Column(sa.Integer, primary_key=True)
    filename = sa.Column(sa.Unicode(1024))

def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    for pasteFile in session.query(PasteFile).all():
        if pasteFile.filename:
            pasteFile.filename = pasteFile.filename.strip()
            session.add(pasteFile)
    session.commit()

def downgrade():
    pass
