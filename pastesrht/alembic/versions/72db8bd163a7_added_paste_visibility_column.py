"""Added paste visibility column

Revision ID: 72db8bd163a7
Revises: 9b2c232477c9
Create Date: 2019-04-14 01:35:13.859096

"""

# revision identifiers, used by Alembic.
revision = '72db8bd163a7'
down_revision = '9b2c232477c9'

from alembic import op
from enum import Enum
import sqlalchemy as sa
import sqlalchemy_utils as sau
from sqlalchemy.dialects import postgresql

class PasteVisibility(Enum):
    public = 'public'
    private = 'private'
    unlisted = 'unlisted'

def upgrade():
    op.add_column('paste', sa.Column('visibility',
        sau.ChoiceType(PasteVisibility, impl=sa.String()), nullable=False,
            server_default='unlisted'))


def downgrade():
    op.drop_column('paste', 'visibility')
