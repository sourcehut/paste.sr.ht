"""Drop legacy webhooks support

Revision ID: 8565f92ed478
Revises: a9cfba1cedba
Create Date: 2021-09-21 14:48:13.225966

"""

# revision identifiers, used by Alembic.
revision = '8565f92ed478'
down_revision = 'a9cfba1cedba'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    DROP TABLE IF EXISTS paste_webhook_delivery;
    DROP TABLE IF EXISTS paste_webhook_subscription;
    """)


def downgrade():
    pass
