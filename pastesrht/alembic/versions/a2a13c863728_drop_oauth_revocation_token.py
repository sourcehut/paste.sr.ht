"""Drop oauth_revocation_token

Revision ID: a2a13c863728
Revises: 5db7585a816b
Create Date: 2024-10-31 13:02:53.642045

"""

# revision identifiers, used by Alembic.
revision = 'a2a13c863728'
down_revision = '5db7585a816b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE "user" DROP COLUMN oauth_revocation_token;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user"
    ADD COLUMN oauth_revocation_token character varying(256);
    """)
