"""Add cascade to paste_file

Revision ID: a9cfba1cedba
Revises: 72db8bd163a7
Create Date: 2021-09-21 14:39:05.297771

"""

# revision identifiers, used by Alembic.
revision = 'a9cfba1cedba'
down_revision = '72db8bd163a7'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE paste_file
    DROP CONSTRAINT paste_file_paste_id_fkey;

    ALTER TABLE paste_file
    ADD CONSTRAINT paste_file_paste_id_fkey
    FOREIGN KEY (paste_id) REFERENCES paste(id)
    ON DELETE CASCADE;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE paste_file
    DROP CONSTRAINT paste_file_paste_id_fkey;

    ALTER TABLE paste_file
    ADD CONSTRAINT paste_file_paste_id_fkey
    FOREIGN KEY (paste_id) REFERENCES paste(id);
    """)
