"""Update user constraints

Revision ID: e8f9e2e911d8
Revises: b18cc88403f0
Create Date: 2025-01-22 15:51:07.761650

"""

# revision identifiers, used by Alembic.
revision = 'e8f9e2e911d8'
down_revision = 'b18cc88403f0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE "user"
    ALTER COLUMN username SET NOT NULL;

    ALTER TABLE "user"
    ADD CONSTRAINT user_email_key
    UNIQUE (email);
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user"
    ALTER COLUMN username DROP NOT NULL;

    ALTER TABLE "user"
    DROP CONSTRIANT user_email_key
    """)
