import humanize
import json
import stat
from pastesrht import filters
from pastesrht.oauth import PasteOAuthService
from srht.config import cfg
from srht.database import DbSession
from srht.flask import SrhtFlask

db = DbSession(cfg("paste.sr.ht", "connection-string"))
db.init()

class PasteApp(SrhtFlask):
    def __init__(self):
        super().__init__("paste.sr.ht", __name__,
                oauth_service=PasteOAuthService())

        self.url_map.strict_slashes = False

        from pastesrht.blueprints.public import public
        from pastesrht.blueprints.api import register_api
        from srht.graphql import gql_blueprint

        self.register_blueprint(public)
        self.register_blueprint(gql_blueprint)
        register_api(self)

        self.add_template_filter(filters.render_markdown)

        @self.context_processor
        def inject():
            return {
                "humanize": humanize,
                "json": json.dumps,
                "stat": stat,
            }


app = PasteApp()
