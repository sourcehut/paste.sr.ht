import pkg_resources
from flask import Blueprint
from pastesrht.blueprints.api.pastes import pastes
from srht.flask import csrf_bypass

def register_api(app):
    app.register_blueprint(pastes)
    csrf_bypass(pastes)

    @app.route("/api/version")
    def version():
        try:
            dist = pkg_resources.get_distribution("pastesrht")
            return { "version": dist.version }
        except:
            return { "version": "unknown" }
