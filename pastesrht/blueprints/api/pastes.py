from flask import Blueprint, abort, request
from hashlib import sha1
from pastesrht.access import get_user_or_abort, has_access, UserAccess
from pastesrht.types import Paste, Blob, PasteFile, PasteVisibility
from srht.api import paginated_response
from srht.database import db
from srht.graphql import exec_gql, GraphQLOperation, GraphQLUpload
from srht.oauth import oauth, current_token
from srht.validation import Validation

pastes = Blueprint("api_pastes", __name__)

@pastes.route("/api/pastes")
@oauth("pastes:read")
def pastes_GET():
    return paginated_response(Paste.sha, Paste.query.filter(Paste.sha != None)
            .filter(Paste.user_id == current_token.user_id))

@pastes.route("/api/pastes/~<user>")
@oauth("pastes:read")
def pastes_user_GET(user):
    user = get_user_or_abort(user)
    return paginated_response(Paste.sha, Paste.query.filter(Paste.sha != None)
            .filter(Paste.user_id == user.id)
            .filter(Paste.visibility == PasteVisibility.public))

@pastes.route("/api/pastes", methods=["POST"])
@oauth("pastes:write")
def pastes_POST():
    valid = Validation(request)
    files = valid.require("files", cls=list)
    if not valid.ok:
        return valid.response
    visibility = valid.optional("visibility",
            cls=PasteVisibility,
            default=PasteVisibility.unlisted)
    filenames = set()
    for i, f in enumerate(files):
        valid.expect(isinstance(f, dict),
                f"Expected files[{i}] to be dict", field=f"files[{i}]")
        contents = f.get("contents")
        filename = f.get("filename")
        valid.expect(contents, f"files[{i}].contents is required",
                field=f"files[{i}]")
        valid.expect(not contents or isinstance(contents, str),
                f"Expceted files[{i}].contents to be a string",
                field=f"files[{i}].contents")
        valid.expect(not filename or isinstance(filename, str),
                f"Expceted files[{i}].filename to be a string",
                field=f"files[{i}].filename")
        valid.expect(filename not in filenames,
                f"Filename {filename or 'none'} cannot be specified more "
                "than once", field=f"files[{i}].filename")
        filenames.update({filename})
    if not valid.ok:
        return valid.response

    op = GraphQLOperation("""
        mutation CreatePaste($files: [Upload!]!, $visibility: Visibility!) {
            create(files: $files, visibility: $visibility) {
                created
                visibility
                sha: id
                user {
                    canonical_name: canonicalName
                    ... on User {
                        name: username
                    }
                }
                files {
                    filename
                    blob_id: hash
                }
            }
        }
    """)
    files = [
        GraphQLUpload(
            file.get("filename"),
            file.get("contents").replace('\r\n', '\n').replace('\r', '\n'),
            "text/plain",
        ) for file in files
    ]
    op.var("files", files)
    op.var("visibility", visibility.value.upper())
    resp = op.execute("paste.sr.ht", user=current_token.user, valid=valid)
    if not valid.ok:
        return valid.response

    resp = resp["create"]
    resp["visibility"] = resp["visibility"].lower()
    return resp, 201

@pastes.route("/api/pastes/<sha>")
@oauth("pastes:read")
def pastes_by_id_GET(sha):
    paste = Paste.query.filter(Paste.sha == sha).one_or_none()
    if not paste:
        abort(404)
    if not has_access(current_token.user, paste, UserAccess.read):
        abort(401)
    return paste.to_dict()

@pastes.route("/api/pastes/<sha>", methods=['PUT'])
@oauth("pastes:write")
def paste_by_id_PUT(sha):
    paste = Paste.query.filter(Paste.sha == sha).one_or_none()
    if not paste:
        abort(404)
    if not has_access(current_token.user, paste, UserAccess.write):
        abort(401)
    valid = Validation(request)
    visibility = valid.optional("visibility",
            cls=PasteVisibility,
            default=paste.visibility)
    resp = exec_gql("paste.sr.ht", """
        mutation UpdatePaste($id: String!, $visibility: Visibility!) {
            update(id: $id, visibility: $visibility) {
                created
                visibility
                sha: id
                user {
                    canonical_name: canonicalName
                    ... on User {
                        name: username
                    }
                }
                files {
                    filename
                    blob_id: hash
                }
            }
        }
    """, user=current_token.user, valid=valid, id=sha, visibility=visibility.value.upper())
    if not valid.ok:
        return valid.response

    resp = resp["update"]
    resp["visibility"] = resp["visibility"].lower()
    return resp

@pastes.route("/api/pastes/<sha>", methods=['DELETE'])
@oauth("pastes:write")
def paste_by_id_DELETE(sha):
    paste = Paste.query.filter(Paste.sha == sha).one_or_none()
    if not paste:
        abort(404)
    if not has_access(current_token.user, paste, UserAccess.write):
        abort(401)
    exec_gql("paste.sr.ht", """
        mutation DeletePaste($id: String!) {
            delete(id: $id) { id }
        }
    """, user=current_token.user, id=sha)
    return {}, 204

@pastes.route("/api/blobs/<sha>")
@oauth("pastes:read")
def blob_by_id_GET(sha):
    blob = Blob.query.filter(Blob.sha == sha).one_or_none()
    if not blob:
        abort(404)
    return blob.to_dict()
