import json
import pygments
from datetime import timedelta
from flask import Blueprint, current_app, render_template, request, redirect, Response
from flask import url_for, abort
from hashlib import sha1
from markupsafe import Markup
from pastesrht.access import get_paste_or_abort, get_user_or_abort, has_access, DbLock, UserAccess
from pastesrht.types import Paste, PasteFile, PasteVisibility, Blob
from pastesrht.search import apply_search
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import guess_lexer, guess_lexer_for_filename, TextLexer
from srht.cache import get_cache, set_cache
from srht.database import db
from srht.flask import paginate_query
from srht.graphql import exec_gql, GraphQLOperation, GraphQLUpload
from srht.oauth import current_user, loginrequired
from srht.validation import Validation

public = Blueprint("public", __name__)

@public.route("/")
def index():
    if current_user:
        return render_template("new-paste.html")
    return render_template("index.html")

def create_paste(valid, files, visibility):
    op = GraphQLOperation("""
        mutation CreatePaste($files: [Upload!]!, $visibility: Visibility!) {
            create(files: $files, visibility: $visibility) { id }
        }
    """)
    uploads = []
    for file in files:
        filename = file.get("filename")
        cache_key = "paste.sr.ht:blobs:{0}".format(file.get("sha"))
        contents = get_cache(cache_key)
        if contents is None:
            abort(404)
        uploads.append(GraphQLUpload(filename, contents.decode(), "text/plain"))
    op.var("files", uploads)
    op.var("visibility", visibility.value.upper())
    resp = op.execute("paste.sr.ht", valid=valid)
    if not valid.ok:
        return None
    return resp["create"]["id"]

@public.route("/new-paste", methods=["POST"])
@loginrequired
def new_paste_POST():
    valid = Validation(request)
    contents = valid.require("contents", friendly_name="File contents")
    if contents:
        contents = contents.replace("\r\n", "\n").replace("\r", "\n")
    filename = valid.optional("filename")
    commit = valid.require("commit")
    visibility = valid.require("visibility", cls=PasteVisibility)
    filename = filename.strip() if filename else filename

    files = valid.optional("files")
    files = json.loads(files) if files else []
    valid.kwargs.pop("files", None)

    def dict_without(d, key):
        new_d = d.copy()
        new_d.pop(key)
        return new_d

    if commit == "force":
        valid.errors = [] # Clear validation errors since contents is not required
        paste_id = create_paste(valid, files, visibility)
        if not valid.ok:
            return render_template("new-paste.html", visibility=visibility,
                    **dict_without(valid.kwargs, "visibility"))
        return redirect(url_for(".paste_GET", user=current_user.username,
                sha=paste_id))

    for f in files:
        if f.get("filename") == filename:
            # TODO: Edit this file?
            valid.error("A file with this name already exists in this paste.",
                    field="filename")
    if not valid.ok:
        return render_template("new-paste.html",
                files=files, visibility=visibility,
                **dict_without(valid.kwargs, "visibility"))

    sha = sha1()
    sha.update(contents.encode())
    sha = sha.hexdigest()

    files.append({
        "sha": sha,
        "filename": filename,
        "size": len(contents),
    })
    set_cache("paste.sr.ht:blobs:{0}".format(sha), timedelta(hours=1), contents.encode())

    if commit == "no":
        return render_template("new-paste.html",
                files=files, visibility=visibility)

    paste_id = create_paste(valid, files, visibility)
    if not valid.ok:
        return render_template("new-paste.html",
                files=files, visibility=visibility,
                **dict_without(valid.kwargs, "visibility"))
    return redirect(url_for(".paste_GET", user=current_user.username,
            sha=paste_id))

def _get_shebang(data):
    if not data.startswith('#!'):
        return None
    endline = data.find('\n')
    if endline == -1:
        shebang = data
    else:
        shebang = data[:endline]
    return shebang

def _get_lexer(name, data):
    try:
        return guess_lexer_for_filename(name, data)
    except pygments.util.ClassNotFound:
        try:
            shebang = _get_shebang(data)
            if not shebang:
                return TextLexer()

            return guess_lexer(shebang)
        except pygments.util.ClassNotFound:
            return TextLexer()

def highlight_file(name, content):
    lexer = _get_lexer(name, content)
    formatter = HtmlFormatter()
    style = formatter.get_style_defs('.highlight')
    html = f"<style>{style}</style>" + highlight(content, lexer, formatter)
    return Markup(html)

@public.route("/~<user>")
def pastes_user(user):
    user = get_user_or_abort(user)
    pastes = (Paste.query
            .filter(Paste.user_id == user.id)
            .filter(Paste.sha != None)
            .order_by(Paste.updated.desc()))
    if (current_user and user.id != current_user.id) or not current_user:
        pastes = pastes.filter(Paste.visibility == PasteVisibility.public)

    try:
        terms = request.args.get("search")
        pastes = apply_search(pastes, terms)
        search_error = None
    except ValueError as e:
        search_error = str(e)

    pastes, pagination = paginate_query(pastes)
    return render_template("pastes.html", pastes=pastes, user=user,
            search=terms, search_error=search_error, **pagination)

@public.route("/~<user>/<sha>")
def paste_GET(user, sha):
    user, paste = get_paste_or_abort(user, sha)
    return render_template("paste.html", paste=paste,
            highlight_file=highlight_file)

@public.route("/~<user>/<sha>/manage")
def paste_manage(user, sha):
    user, paste = get_paste_or_abort(user, sha)
    if not has_access(current_user, paste, UserAccess.write):
        abort(401)

    return render_template("paste-manage.html", paste=paste)

@public.route("/~<user>/<sha>/manage", methods=['POST'])
def paste_manage_POST(user, sha):
    user, paste = get_paste_or_abort(user, sha)
    if not has_access(current_user, paste, UserAccess.write):
        abort(401)

    valid = Validation(request)
    visibility = valid.optional("visibility",
            cls=PasteVisibility,
            default=paste.visibility)

    exec_gql(current_app.site, """
        mutation UpdatePaste($id: String!, $visibility: Visibility!) {
            update(id: $id, visibility: $visibility) { id }
        }
    """, id=sha, visibility=visibility.value.upper())

    return redirect(url_for('.paste_manage', user=user.username, sha=sha))

@public.route("/~<user>/<sha>/delete")
def paste_delete(user, sha):
    user, paste = get_paste_or_abort(user, sha)
    if not has_access(current_user, paste, UserAccess.write):
        abort(401)

    return render_template("paste-delete.html", paste=paste)

@public.route("/~<user>/<sha>/delete", methods=['POST'])
def paste_delete_POST(user, sha):
    user, paste = get_paste_or_abort(user, sha)
    if not has_access(current_user, paste, UserAccess.write):
        abort(401)

    exec_gql(current_app.site, """
        mutation DeletePaste($id: String!) {
            delete(id: $id) { id }
        }
    """, id=sha)
    return redirect(url_for('.index'))

@public.route("/blob/<sha>")
def blob_GET(sha):
    blob = Blob.query.filter(Blob.sha == sha).one_or_none()
    if not blob:
        abort(404)
    return Response(blob.contents, mimetype="text/plain")

@public.route("/pastes")
@loginrequired
def pastes():
    return redirect(url_for('.pastes_user', user=current_user.username))
