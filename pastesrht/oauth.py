from srht.config import cfg
from srht.oauth import AbstractOAuthService, DelegatedScope
from pastesrht.types import User, OAuthToken

client_id = cfg("paste.sr.ht", "oauth-client-id")
client_secret = cfg("paste.sr.ht", "oauth-client-secret")

class PasteOAuthService(AbstractOAuthService):
    def __init__(self):
        super().__init__(client_id, client_secret, delegated_scopes=[
            DelegatedScope("pastes", "pastes", True),
        ], user_class=User, token_class=OAuthToken)
