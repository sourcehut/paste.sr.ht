from srht.database import Base
from srht.oauth import ExternalUserMixin, ExternalOAuthTokenMixin
from pastesrht.types.blob import Blob
from pastesrht.types.paste import Paste, PasteFile, PasteVisibility

class User(Base, ExternalUserMixin):
    pass

class OAuthToken(Base, ExternalOAuthTokenMixin):
    pass
