import sqlalchemy as sa
import sqlalchemy_utils as sau
from srht.database import Base
from enum import Enum

class PasteVisibility(Enum):
    public = 'public'
    private = 'private'
    unlisted = 'unlisted'

class Paste(Base):
    __tablename__ = 'paste'
    id = sa.Column(sa.Integer, primary_key=True)
    created = sa.Column(sa.DateTime, nullable=False)
    updated = sa.Column(sa.DateTime, nullable=False)
    sha = sa.Column(sa.String(40), index=True) # null for incomplete pastes
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    user = sa.orm.relationship('User', backref=sa.orm.backref('pastes'))
    visibility = sa.Column(
            sau.ChoiceType(PasteVisibility, impl=sa.String()),
            nullable=False,
            server_default='unlisted')

    def to_dict(self):
        return {
            "created": self.created,
            "visibility": self.visibility,
            "sha": self.sha,
            "user": self.user.to_dict(short=True),
            "files": [
                f.to_dict(short=True) for f in self.files
            ],
        }

class PasteFile(Base):
    __tablename__ = 'paste_file'
    id = sa.Column(sa.Integer, primary_key=True)
    created = sa.Column(sa.DateTime, nullable=False)
    updated = sa.Column(sa.DateTime, nullable=False)
    filename = sa.Column(sa.Unicode(1024))

    blob_id = sa.Column(sa.Integer,
            sa.ForeignKey('blob.id'), nullable=False)
    blob = sa.orm.relationship("Blob")

    paste_id = sa.Column(sa.Integer,
            sa.ForeignKey('paste.id', ondelete="CASCADE"),
            nullable=False)
    paste = sa.orm.relationship("Paste",
            backref=sa.orm.backref("files",
                cascade="save-update, merge, delete"))

    def to_dict(self, short=False):
        return {
            "filename": self.filename,
            "blob_id": self.blob.sha,
        }
